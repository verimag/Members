This project is meant to make the `veri-gitlabrun` runner available to Verimag members.

To be able to use this runner, you need to asks to one of the project member with `Maintainer` status to add you and assign you the `Maintainer` status 

Once this is done, you should be able to enable `veri-gitlabrun` in all
 your projects (via `Settings->CI/CD->Runner settings`)
 
`veri-gitlabrun` is hosted on a (virtual) debian box with 10 Xeon @ 2.00GHz and 4M of RAM.
It can run 10 jobs simultaneously.

